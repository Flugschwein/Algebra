Hier ist meine Mitschrift in der Algebra
[Algebra](https://ufind.univie.ac.at/de/course.html?lv=250077&semester=2022W)
Vorlesung an der Universität Wien im Wintersemester 2022.

Danke an alle aufmerksamen Leser\*innen, die mich auf Tippfehler und
dergleichen hinweisen.
